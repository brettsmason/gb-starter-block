//  Import CSS
import './style.scss';
import './editor.scss';

// Setup variables
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'tnc/starter-block', {
	title: __( 'Starter Block' ),
	icon: 'shield',
	category: 'common',
	keywords: [
		__( 'starter' ),
	],

	edit: function( props ) {
		return (
			<div className={ props.className }>
				<p>— Hello from the backend.</p>
			</div>
		);
	},

	save: function( props ) {
		return (
			<div className={ props.className }>
				<p>— Hello from the frontend.</p>
			</div>
		);
	},
} );
